from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from login.forms import SignupForm
from login.models import UserProfile
from .forms import EditProfileForm
from sellingform.models import Item
from description.models import Bid
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from login.models import UserProfile

from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm

from django.contrib.auth import update_session_auth_hash
from django.core import serializers
from django.http import HttpResponse, JsonResponse
import requests
import json

# from django.contrib.auth.decorators import login_required

# Create your views here.

def view_profile(request):
	args = {
		'user': request.user,
		'userdata': UserProfile.objects.get(user=request.user),
		'items' : Item.objects.filter(user=request.user),
		'bids' : Bid.objects.filter(user=request.user),
		
		}
		
	return render(request, 'profile.html', args)


def edit_profile(request):
	if request.method =="POST":
		form = EditProfileForm(request.POST, instance=request.user)

		if form.is_valid():
			form.save()
			return redirect('/')

	else:
		form = EditProfileForm(instance=request.user)
		args = {'form': form}
		return render(request, 'edit_profile.html', args)


def change_password (request):
	if request.method == 'POST':
		form = PasswordChangeForm(data=request.POST, user=request.user)

		if form.is_valid():
			form.save()
			update_session_auth_hash(request, form.user)
			return redirect('/')

		else:
			return redirect('/profile/change-password')


	else:
		form = PasswordChangeForm(user=request.user)
		args = {'form': form}
		return render(request, 'change_password.html', args)

# def parseData(request):
# 	queryResult = []

# 	for item in Item.objects.filter(user=request.user):
# 		queryResult.append(item)

# 	# for bid in Bid.objects.filter(user=request.user):
# 	# 	queryResult.append(bid)

# 	item_list = serializers.serialize('json',queryResult)
# 	return HttpResponse(item_list, content_type="application/json")

# def getData(request):
# 	url = 'http://127.0.0.1:8000/profile/parseData/' 
# 	response = requests.get(url).json()
# 	print(response)
# 	return JsonResponse(response, safe=False)