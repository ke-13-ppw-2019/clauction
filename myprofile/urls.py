from django.urls import path
from myprofile import views

app_name = 'myprofile'

urlpatterns = [
	path('', views.view_profile, name='view_profile'),
	path('edit', views.edit_profile, name='edit_profile'),
	path('change-password', views.change_password, name='change_password'),
	#path('parseData/',views.parseData,name='parseData'),
    #path('getData/',views.getData,name='getData')
]