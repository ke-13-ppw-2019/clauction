from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from login.models import UserProfile

# Create your tests here.
class ProfileUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
        User.objects.create(
            first_name = "Wulan", 
            last_name = "Mantiri",
            username = "wulanmantiri_",
            password = "qwertyuiop098",
            email = "wulan.mantiri@gmail.com",)
        UserProfile.objects.create(
            user = User.objects.get(username="wulanmantiri_"),
            image = "assets/ferarri.jpg",
            phone_number = "08228888888",
            address = "pacil",
            birthday = "2000-03-01",
            agree = True,
        )
        self.client.force_login(User.objects.get(username = "wulanmantiri_"))            
        self.form_page_content = self.client.get('/profile/').content.decode('utf8')
        
    def test_apakah_url_profile_ada(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_url_edit_profile_ada(self):
        response = self.client.get('/profile/edit')
        self.assertEqual(response.status_code, 200)
    
    def test_apakah_view_profile_html_ditampilkan(self):
        response = self.client.get("/profile/")
        self.assertTemplateUsed(response, 'profile.html')
    
    def test_apakah_edit_profile_html_ditampilkan(self):
        response = self.client.get('/profile/edit')
        self.assertTemplateUsed(response, 'edit_profile.html')