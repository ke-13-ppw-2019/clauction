from . import views
from django.urls import path

app_name = 'home'

urlpatterns = [
    path('', views.home, name='home'),
    path('parseData/<str:query>',views.parseData,name='parseData'),
    path('getData/',views.getData,name='getData'),
    path('parseDataAll/',views.parseDataAll,name='parseDataAll'),
    path('getDataAll/',views.getDataAll,name='getDataAll'),
    path('review/',views.review,name='review')
]