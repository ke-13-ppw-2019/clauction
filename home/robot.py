from sellingform.models import Item
from description.models import Bid
from django.contrib.auth.models import User
import datetime

def update_price_robot():
    for obj in Item.objects.all():
        enddate = datetime.datetime(obj.end_date.year,obj.end_date.month, obj.end_date.day, obj.end_date.hour)
        daysleft = (enddate - datetime.datetime.now()).days
        hoursleft = (enddate - datetime.datetime.now()).seconds//3600
        lastbid = obj.bids.last()
        if ((lastbid == None) and (obj.price==0)):
            obj.price = obj.starting_price
        elif (lastbid != None) and (lastbid.price > obj.price):
            obj.price = lastbid.price

        if(daysleft<0):
            obj.status = 'Sold out'
            obj.save()
            continue

        obj.price = obj.price + 5
        obj.save()


        # Customer.objects.create(
        #     item=obj, 
        #     name="Robot", 
        #     price=obj.price, 
        #     address="Dunia lain", 
        #     phone_number=4040404, 
        #     terms_confirm=True)

        numberOfUsersNow = User.objects.all().count()

        newUsername = "robotrobot_" + str(numberOfUsersNow+1)

        User.objects.create(
            #id = numberOfUsersNow+1,
            first_name = "Robot", 
            last_name = "Robot",
            username = newUsername,
            password = "qwertyuiop098",
            email = "iamrobot@gmail.com",
        )

        # UserProfile.objects.create(
        #     id = numberOfUsersNow+1,
        #     user = User.objects.get(id=numberOfUsersNow+1),
        #     phone_number = "081234567890",
        #     address = "Fasilkom UI",
        #     birthday = datetime.datetime.today(),
        #     agree = True, )

        Bid.objects.create(
            item = obj,
            user = User.objects.get(username=newUsername),
            price = obj.price,
        )