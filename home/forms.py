from django import forms
from .models import Review

attrs = {
    'class': 'form-control form-group' 
}

sort_by_choices = (
    ('Newest',"Newest"),
    ('Oldest', "Oldest"),
    ('Alphabetical',"Alphabetical"),
    ('Price (from lowest)',"Price (from lowest)"),
    ('Price (from highest)',"Price (from highest)"),
    ('End Date (from nearest)',"End Date (from nearest)"),
    ('End Date (from farthest)',"End Date (from farthest)")
)

class SortByForm(forms.Form):
    sort_by =  forms.ChoiceField(label='Sort by',widget=forms.Select(attrs=attrs),choices=sort_by_choices, initial="Newest")

class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        fields = [
            'review_content',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
                'placeholder' : 'Write your review here...'
            })
