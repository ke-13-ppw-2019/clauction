from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class SortBy(models.Model):

    class Meta:
        verbose_name_plural = 'Sort-by(s)'

    sort_by_choice = models.CharField(max_length=30)
    lastSort = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sort_by_choice

class Review(models.Model):

    review_content = models.CharField(max_length=300)
    reviewer_username = models.CharField(max_length=300,default="")
    no = models.CharField(max_length=10,default="")
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.review_content