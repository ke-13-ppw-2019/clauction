from django.test import TestCase, Client
from .models import SortBy
from sellingform.models import Item
from .forms import SortByForm
from description.models import Bid
from . import robot
from django.contrib.auth.models import User
import datetime
from django.contrib import auth
from .models import Review
import json

# Create your tests here.
class SortByModelTest(TestCase):

    def test_string_representation(self):
        entry = SortBy(sort_by_choice='choice')
        self.assertEqual(str(entry),entry.sort_by_choice)

    def test_verbose_name_plural(self):
        self.assertEqual(str(SortBy._meta.verbose_name_plural), "Sort-by(s)")

class HomePageTest(TestCase):

    def setUp(self):
        self.client = Client()
        User.objects.create(
            first_name = "Wulan", 
            last_name = "Mantiri",
            username = "wulanmantiri_",
            password = "qwertyuiop098",
            email = "wulan.mantiri@gmail.com",)
        Item.objects.create(
            user = User.objects.get(username="wulanmantiri_"),
            #id = 1,  
            bank_name = "BCA", 
            bank_account = "12345678",
            title = "chevrolet", 
            start_date = datetime.datetime.now(), 
            end_date = "2019-12-31 17:00:00+00:00",
            starting_price = "15", 
            price="0", 
            image = "assets/ferrari.jpg", 
            description = "good", )
        Item.objects.create(
            user = User.objects.get(username="wulanmantiri_"),
            #id = 2,
            bank_name = "bca", 
            bank_account = "12345678",
            title = "avanza", 
            start_date = datetime.datetime.now(), 
            end_date = "2018-12-31 17:00:00+00:00",
            starting_price = "25", price="0", 
            image = "assets/ferrari.jpg", 
            description = "very good", )
        self.desc_page_content = self.client.get('/1/chevrolet').content.decode('utf8')
        self.avanza_page_content = self.client.get('/2/avanza').content.decode('utf8')
        self.client.force_login(User.objects.get(username = "wulanmantiri_"))

    def test_if_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'home.html')

    def test_if_homepage_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    # def test_if_goods_is_empty(self):
    #     response = Client().get('/')
    #     content = response.content.decode('utf8')
    #     Item.objects.all().delete()
    #     item = Item.objects.all()
    #     if(len(item)==0):
    #         self.assertIn("Place your first items here!",content)

    def test_if_goods_exists(self):
        # name = 'test'
        # address = 'dimana'
        # phone_number = 923
        # email = 'gjf@gmail.com'
        # bank_name = 'bni'
        # bank_account = '39223'
        # title = 'barang'
        # end_date = "2019-12-10 17:00:00+00:00"
        # starting_price = 10
        # image = '/media/images/chevrolet.jpg'
        # description = 'beli dong'
        # agree = True
        # price = 10
        # status = 'Available'
        # SellerModel.objects.create(
        #     name=name,
        #     address=address,
        #     phone_number=phone_number,
        #     email=email,
        #     bank_name=bank_name,
        #     bank_account=bank_account,
        #     title=title,
        #     end_date=end_date,
        #     starting_price=starting_price,
        #     image=image,
        #     description=description,
        #     agree=agree,
        #     price=price,
        #     status=status
        # )
        response = Client().get('/')
        content = response.content.decode('utf8')
        hitungjumlah = Item.objects.all().count()
        self.assertEqual(hitungjumlah,2)
        self.assertIn('flex-wrap',content)
        self.assertIn('card-barang',content)
        """
        if(SellerModel.objects.all().count):
            self.assertIn('flex-wrap', content)
            self.assertIn('card-barang',content)
        """

    def test_if_goods_properties_exists(self):
        # name = 'test'
        # address = 'dimana'
        # phone_number = 923
        # email = 'gjf@gmail.com'
        # bank_name = 'bni'
        # bank_account = '39223'
        # title = 'barang'
        # end_date = "2019-12-10 17:00:00+00:00"
        # starting_price = 10
        # image = '/media/images/chevrolet.jpg'
        # description = 'beli dong'
        # agree = True
        # price = 10
        # status = 'Available'
        # SellerModel.objects.create(
        #     name=name,
        #     address=address,
        #     phone_number=phone_number,
        #     email=email,
        #     bank_name=bank_name,
        #     bank_account=bank_account,
        #     title=title,
        #     end_date=end_date,
        #     starting_price=starting_price,
        #     image=image,
        #     description=description,
        #     agree=agree,
        #     price=price,
        #     status=status
        # )
        response = Client().get('/')
        content = response.content.decode('utf8')
        
        obj = Item.objects.get(id=1)
        self.assertIn(obj.title, content)
        self.assertIn(str(obj.starting_price+5), content)
        #self.assertIn(obj.status, content)

    def test_is_sort_by_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Sort by',content)
        self.assertIn('<option value="Newest"',content)
        self.assertIn('<option value="Oldest"',content)
        self.assertIn('<option value="Alphabetical"',content)
        self.assertIn('<option value="Price (from lowest)"',content)
        self.assertIn('<option value="Price (from highest)"',content)
        self.assertIn('<option value="End Date (from nearest)"', content)
        self.assertIn('<option value="End Date (from farthest)"', content)
        self.assertIn('<button class="sort-button"', content)

    def test_is_navbar_and_the_contents_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('nav',content)
        self.assertIn('Home',content)
        self.assertIn('Want to Sell?',content)
        self.assertIn('clauction-logo.png',content)

    def test_is_last_sorted_by_exists(self):
        # name = 'test'
        # address = 'dimana'
        # phone_number = 923
        # email = 'gjf@gmail.com'
        # bank_name = 'bni'
        # bank_account = '39223'
        # title = 'barang'
        # end_date = "2019-12-10 17:00:00+00:00"
        # starting_price = 10
        # image = '/media/images/chevrolet.jpg'
        # description = 'beli dong'
        # agree = True
        # price = 10
        # status = 'Available'
        # SellerModel.objects.create(
        #     name=name,
        #     address=address,
        #     phone_number=phone_number,
        #     email=email,
        #     bank_name=bank_name,
        #     bank_account=bank_account,
        #     title=title,
        #     end_date=end_date,
        #     starting_price=starting_price,
        #     image=image,
        #     description=description,
        #     agree=agree,
        #     price=price,
        #     status=status
        # )
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Last sorted by',content)

    def test_if_intro_background_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('logo-horizontal.png',content)
        self.assertIn('Best place to bid.',content)

    def test_if_robot_works_without_last_bid(self):
        # name = 'test'
        # address = 'dimana'
        # phone_number = 923
        # email = 'gjf@gmail.com'
        # bank_name = 'bni'
        # bank_account = '39223'
        # title = 'barang'
        # end_date = "2019-12-10 17:00:00+00:00"
        # starting_price = 10
        # image = '/media/images/chevrolet.jpg'
        # description = 'beli dong'
        # agree = True
        # price = 0
        # status = 'Available'
        # SellerModel.objects.create(
        #     name=name,
        #     address=address,
        #     phone_number=phone_number,
        #     email=email,
        #     bank_name=bank_name,
        #     bank_account=bank_account,
        #     title=title,
        #     end_date=end_date,
        #     starting_price=starting_price,
        #     image=image,
        #     description=description,
        #     agree=agree,
        #     price=price,
        #     status=status
        # )
        response = Client().get('/')
        obj = Item.objects.get(id=1)
        #robot.update_price_robot()
        content = response.content.decode('utf8')
        self.assertIn(str(obj.starting_price+5),content)

    def test_if_robot_works_with_last_bid(self):
        # name = 'test'
        # address = 'dimana'
        # phone_number = 923
        # email = 'gjf@gmail.com'
        # bank_name = 'bni'
        # bank_account = '39223'
        # title = 'barang'
        # end_date = "2019-12-10 17:00:00+00:00"
        # starting_price = 10
        # image = '/media/images/chevrolet.jpg'
        # description = 'beli dong'
        # agree = True
        # price = 0
        # status = 'Available'
        # SellerModel.objects.create(
        #     name=name,
        #     address=address,
        #     phone_number=phone_number,
        #     email=email,
        #     bank_name=bank_name,
        #     bank_account=bank_account,
        #     title=title,
        #     end_date=end_date,
        #     starting_price=starting_price,
        #     image=image,
        #     description=description,
        #     agree=agree,
        #     price=price,
        #     status=status
        # )

        obj = Item.objects.get(id=1)
        Bid.objects.create(
            item = obj,
            user = User.objects.get(username="wulanmantiri_"),
            price = 37,
        )
        bids = Bid.objects.get(id=1)
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn(str(bids.price+5),content)

#     def test_if_robot_works_sold_out(self):
#         name = 'test'
#         address = 'dimana'
#         phone_number = 923
#         email = 'gjf@gmail.com'
#         bank_name = 'bni'
#         bank_account = '39223'
#         title = 'barang'
#         end_date = "2019-05-05 17:00:00+00:00"
#         starting_price = 10
#         image = '/media/images/chevrolet.jpg'
#         description = 'beli dong'
#         agree = True
#         price = 0
#         status = 'Available'
#         SellerModel.objects.create(
#             name=name,
#             address=address,
#             phone_number=phone_number,
#             email=email,
#             bank_name=bank_name,
#             bank_account=bank_account,
#             title=title,
#             end_date=end_date,
#             starting_price=starting_price,
#             image=image,
#             description=description,
#             agree=agree,
#             price=price,
#             status=status
#         )
#         response = Client().get('/')
#         content = response.content.decode('utf8')
#         self.assertIn('Sold out',content)

    def test_if_sort_by_form_valid(self):
        form = SortByForm(data={'sort_by': 'Newest'})
        self.assertTrue(form.is_valid())

    def test_if_sort_by_form_works_by_newest(self):
        test = 'Newest'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_oldest(self):
        test = 'Oldest'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_alphabetical(self):
        test = 'Alphabetical'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_lowest_price(self):
        test = 'Price (from lowest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_highest_price(self):
        test = 'Price (from highest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_nearest_end_date(self):
        test = 'End Date (from nearest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_sort_by_form_works_by_farthest_end_date(self):
        test = 'End Date (from farthest)'
        response_post = Client().post('/',{'sort_by':test})
        self.assertTemplateUsed('home.html')

    def test_if_user_already_logged_in(self):

        agree = True
        Client().post('/signup',data={
            "username":"testhahihu",
            "password1":"halohalobandung",
            "password2":"halohalobandung",
            "email":"hehehe@gmail.com",
            "first_name":"kalo gamau",
            "last_name":"gimana",
            "phone_number":"081234567",
            "address":"jalan mamasuka",
            "birthday":"2000-03-01",
            "agree":agree,
        })

        self.assertTemplateUsed('home.html')

    def test_if_user_not_logged_in(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('Log In',content)

    def test_if_search_bar_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn('<input id="search"',content)

    # def test_get_data(self):
    #     response = Client().get('/getData/?q=chevrolet')
    #     self.assertTemplateUsed('home.html')
    #     self.assertEqual(response.status_code, 200)
        
    def test_get_data_all(self):
        response = Client().get('/getDataAll')
        self.assertTemplateUsed('home.html')
        self.assertEqual(response.status_code, 301)
       
    def test_parse_data(self):
        response = Client().get('/parseData/chevrolet')
        self.assertTemplateUsed('home.html')
        self.assertEqual(response.status_code, 200)
        
    def test_parse_data_all(self):
        response = Client().get('/parseDataAll')
        self.assertTemplateUsed('home.html')
        self.assertEqual(response.status_code, 301)
        
    def test_if_review_bar_exists(self):
        response = Client().get('/')
        content = response.content.decode('utf8')
        self.assertIn("What they say about Clauction",content)

    def test_review_model(self):
        Review.objects.create(
            review_content = "Bagus banget parah!",
            reviewer_username = "glendaesutanto",
        )
        self.assertEqual(Review.objects.all().count(), 1)

    def test_review_string_representation(self):
        review = Review(
            review_content = "Bagus banget parah!",
            reviewer_username = "glendaesutanto",
        )

        self.assertEqual(str(review),review.review_content)

    def test_review(self):
        response = Client().post('/review',data={'review_content':'bagus banget ihhhh!!!!!'}, content_type="application/json")
        #self.assertEqual(Review.objects.all().count(),1)
        self.assertTemplateUsed('home.html')
        self.assertEqual(response.status_code, 301)
