from django.contrib import admin
from .models import SortBy
from .models import Review

# Register your models here.
admin.site.register(SortBy)
admin.site.register(Review)