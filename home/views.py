from django.shortcuts import render, redirect
from .forms import SortByForm
from sellingform.models import Item
from .models import SortBy
from description.models import Bid
from django.urls import reverse
from . import robot
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.forms import AuthenticationForm
import requests
from .models import Review
from .forms import ReviewForm
import json
from django.contrib.auth.models import User

def home(request):
    if request.method == 'POST':
        form = SortByForm(request.POST)
        if form.is_valid():
            this_choice = request.POST.get('sort_by')
            create_object(this_choice)
            
            lastSortObject = SortBy.objects.last()
            get_objects = get_objects_sorted_by(this_choice)
           
            response = {
                'title' : 'Clauction',
                'form' : form,
                'loginform' : AuthenticationForm(),
                'seller' : get_objects_sorted_by(this_choice),
                'last' : lastSortObject,
                'sellorbuy' : 'Sell',
                'navlink' : reverse('sell'),
                'review_form' : ReviewForm(),
                'reviews' : Review.objects.all(),
            }
            return render(request,("home.html"),response)
    else:
        form = SortByForm()
        create_object('Newest')
        lastSortObject = SortBy.objects.last()

        robot.update_price_robot()

    response = {
        'title' : 'Clauction',
        'form' : form,
        'loginform' : AuthenticationForm(),
        'seller' : get_objects_sorted_by('Newest'),  
        'last' : lastSortObject,
        'sellorbuy' : 'Sell',
        'navlink' : reverse('sell'),
        'review_form' : ReviewForm(),
        'reviews' : Review.objects.all(),

    }
    return render(request,("home.html"),response)

def create_object(curr_choice):
    sortbyobjects = SortBy(sort_by_choice=curr_choice)
    sortbyobjects.save()

def get_objects_sorted_by(choice):
    if(choice=='Newest'):
        return Item.objects.all().order_by('-start_date')
    elif(choice=='Oldest'):
        return Item.objects.all().order_by('start_date')
    elif(choice=='Alphabetical'):
        return Item.objects.all().order_by('title')
    elif(choice=='Price (from lowest)'):
        return Item.objects.all().order_by("price")
    elif(choice=='Price (from highest)'):
        return Item.objects.all().order_by("-price")
    elif(choice=='End Date (from nearest)'):
        return Item.objects.all().order_by("end_date")
    elif(choice=='End Date (from farthest)'):
        return Item.objects.all().order_by('-end_date')

def parseData(request, query):
    queryResult = []

    for item in Item.objects.all():
        if(query.lower() in item.title.lower()):
            queryResult.append(item)

    item_list = serializers.serialize('json',queryResult)
    return HttpResponse(item_list, content_type="application/json")

def parseDataAll(request):
    item = Item.objects.all()

    item_list = serializers.serialize('json',item)
    return HttpResponse(item_list, content_type="application/json")

def getDataAll(request):
    url = 'http://clauction.herokuapp.com/parseDataAll/'

    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)

def getData(request):
    q = request.GET['q']
    url = 'http://clauction.herokuapp.com/parseData/' +  q

    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)

def review(request):
    if request.method == "POST":
        data = json.loads(request.body)
        form = ReviewForm({'review_content' : data['review_content']})
        if form.is_valid():
            banyak = Review.objects.all().count()
            #print('valid eheh')
            author = User.objects.get(username=request.session['username'])
            #print('uname: ' + author.username)
            item = form.save(commit=False)
            item.reviewer_username = author.username
            item.no = str(banyak+1)
            item.save()
            print(Review.objects.last().review_content)
            return JsonResponse({
                'review_content' : Review.objects.last().review_content,
                'author' : Review.objects.last().reviewer_username,
                'no' : str(banyak+1),
                'created_at' : Review.objects.last().created_at,
            })
        #print('askfjakfj')
        print(form.errors)