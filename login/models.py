from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")
    image = models.ImageField(upload_to="profile-pics", blank=True)
    phone_number = models.PositiveIntegerField(default=0)
    address = models.TextField()
    birthday = models.DateField()
    agree =  models.BooleanField(blank=False, default=False)