from django.urls import path
from .views import login_page, signup, logout_page

app_name = 'login'

urlpatterns = [
    path('login', login_page, name='login'),
    path('signup', signup, name='signup'),
    path('logout', logout_page, name='logout'),
]
