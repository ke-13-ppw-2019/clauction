from django.test import TestCase, Client
from django.contrib.auth.models import User

class LoginUnitTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_url_login_exists(self):
        self.response = self.client.get('/login')
        self.page_content = self.response.content.decode('utf8')
        self.assertEqual(self.response.status_code, 200)
    
    # def test_url_logout_exists(self):
    #     self.response = self.client.get('/logout')
    #     self.page_content = self.response.content.decode('utf8')
    #     self.assertEqual(self.response.status_code, 302)
    #     self.assertEqual(self.response['location'], '/')
    
    def test_url_signup_exists(self):
        self.response = self.client.get('/signup')
        self.page_content = self.response.content.decode('utf8')
        self.assertEqual(self.response.status_code, 200)

    def test_signup(self):
        agree = True
        Client().post('/signup',data={
            "username":"testhahihu",
            "password1":"halohalobandung",
            "password2":"halohalobandung",
            "email":"hehehe@gmail.com",
            "first_name":"kalo gamau",
            "last_name":"gimana",
            "phone_number":"081234567",
            "address":"jalan mamasuka",
            "birthday":"2000-03-01",
            "agree":agree,
        })
        self.assertEqual(User.objects.all().count(),1)
    
    def test_login(self):
        agree = True
        Client().post('/signup',data={
            "username":"testhahihu",
            "password1":"halohalobandung",
            "password2":"halohalobandung",
            "email":"hehehe@gmail.com",
            "first_name":"kalo gamau",
            "last_name":"gimana",
            "phone_number":"081234567",
            "address":"jalan mamasuka",
            "birthday":"2000-03-01",
            "agree":agree,
        })
        Client().post('/login',data={"username":"testhahihu","password":"halohalobandung"})

    def test_logout(self):
        Client().post('/logout',data={"username":"testhahihu","password":"halohalobandung"})