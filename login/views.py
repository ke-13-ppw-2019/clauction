from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .forms import SignupForm

def login_page(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            request.session['username'] = user.username
            return redirect('home:home')
    else:
        form = AuthenticationForm()
    return render(request, 'login.html', {'form':form, 'errors':form.errors})

def signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST, request.FILES)
        if form.is_valid():
            print(request.FILES.get('image'))
            user = form.save(file = request.FILES.get('image'))
            login(request, user)
            request.session['username'] = user.username
            return redirect('home:home')
    else:
        form = SignupForm()

    context = {
        'errors' : form.errors,
        'form': form
    }
    return render(request, 'signup.html', context)
    
def logout_page(request):
    if request.method == 'POST':
        logout(request)
        request.session.flush()
        return redirect('home:home')