from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import UserProfile

email_attrs = { 'placeholder': 'someone@email.com', }

name1_attrs = { 'placeholder': 'First Name', }

name2_attrs = { 'placeholder': 'Last Name', }

address_attrs = { 'placeholder': 'Building (Room #), Road #, City, State, Country', }

class DateInput(forms.DateInput):
    input_type='date'

class SignupForm(UserCreationForm):
    email = forms.EmailField(required=True, label='Email', widget=forms.TextInput(attrs=email_attrs))
    first_name = forms.CharField(required=True, label='First Name', max_length=200, widget=forms.TextInput(attrs=name1_attrs))
    last_name = forms.CharField(required=True, label='Last Name', max_length=200, widget=forms.TextInput(attrs=name2_attrs))
    phone_number = forms.IntegerField(required=True, label='Phone Number', widget=forms.NumberInput())
    image = forms.ImageField(label='Profile Picture', allow_empty_file=True, required=False)
    address = forms.CharField(label='Address', required=False, max_length=500,  widget=forms.TextInput(attrs=address_attrs))
    birthday = forms.DateField(label='Birthday', required=True, widget=DateInput())
    agree = forms.BooleanField(label='I have read and agreed to the Terms and Conditions.', required=True, widget=forms.CheckboxInput())

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'birthday', 'username', 'email', 'password1', 'password2',
                'address', 'phone_number', 'image', 'agree',)

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs['placeholder'] = "someone123"
        self.fields['password2'].label = "Confirm Password"

    def save(self, file, commit=True):
        user = super(SignupForm, self).save(commit=False)
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
            UserProfile.objects.create(
                user = user,
                birthday = self.cleaned_data["birthday"],
                address = self.cleaned_data["address"],
                phone_number = self.cleaned_data["phone_number"],
                agree = self.cleaned_data["agree"],
                image = file,
            )
        return user
