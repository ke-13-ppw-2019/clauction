from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

from django.conf import settings
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include('home.urls')),
    path('', include('description.urls')),
    path('', include('login.urls')),
    path('sell/', include('sellingform.urls')),
    path('', include('wishlist.urls')),
    path('profile/', include('myprofile.urls')),
    ]

if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$',
        serve, {'document_root':
        settings.MEDIA_ROOT, }),
    ]
