function writeContent() {
    event.preventDefault(true);

    var key = $('#id_review_content').val();
    var csrftoken = $('input[name="csrfmiddlewaretoken"]').attr('value');

    console.log("key = " + key);

    $.ajax({
        method: 'POST',
        url: 'http://clauction.herokuapp.com/review/',
        dataType: 'json',
        data: JSON.stringify({
            review_content: key
        }),
        beforeSend: function (request) {
            request.setRequestHeader('X-CSRFToken', csrftoken)
        },
        success: function (response) {
            console.log(response.review_content)

            var div = document.createElement("div")

            var div1 = document.createElement("div")
            div1.classList.add("review-content")
            div1.innerText = response.no + ". " + response.review_content

            var div2 = document.createElement("div")
            div2.classList.add("review-author")
            div2.innerText = "by " + response.author + ", at " + response.created_at

            div.appendChild(div1)
            div.appendChild(div2)

            $('#truly_result').append(div)
        }
    });
}