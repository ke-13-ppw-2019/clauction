$('#login_button').on({
    click: function(){
        $('#overlay_login').css("display", "flex");
    },
}); 

$('#login_form').on({
    mouseleave: function(){
        $('#overlay_login').css("display", "none");
    },
}); 

$(document).on('submit', '#login_form', function() {
    $.ajax({
        method: 'POST',
        url : "{% url 'home:home' %}" ,
        data: {
            username : $('input[name=username]').val(), 
            password : $('input[name=password]').val(), 
            csrftoken: $('input[name=csrfmiddlewaretoken]').val(),
        },
        success: function(json) {
            $('#overlay_login').css("display", "flex");
            $('#thanks_content').css("display", "flex");
        }
    });
}); 