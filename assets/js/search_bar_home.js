$(document).ready(() => {
    $('#search').on("keyup", function () {
        var query = $('#search').val();
        var getUrl = '';

        if (query.length == 0) {
            getUrl = 'http://clauction.herokuapp.com/getDataAll/'
        } else {
            getUrl = 'http://clauction.herokuapp.com/getData/?q=' + query
        }

        //console.log('query = ' + query + ', panjang = ' + query.length);
        //console.log('url = ' + getUrl);

        $.ajax({
            method: 'GET',
            url: getUrl,
            success: function (response) {
                $('#result').empty();

                if (response.length == 0) {
                    var trHTML = '<div style="margin: 30px;">';
                    trHTML += '<div style="text-align: center;"><h3>No items found!</h3></div></div>';
                    $('#result').append(trHTML);
                } else {
                    var trHTML = '<div class="flex-wrap">';
                    for (let i = 0; i < response.length; i++) {
                        trHTML += '<a href="/' + response[i].pk + '/' + response[i].fields.title + '">' + '<div class="card-barang"><img src="/media/' + response[i].fields.image + '" width="250" height="180" class="gambar" style="margin-top: 0; border-radius: 20px 20px 0px 0px;"/><div class="info-barang"><div style="margin: 15px;"></div><div class="judul-barang"><a href="/' + response[i].pk + '/' + response[i].fields.title + '"><b>' + response[i].fields.title + '</b></a><div style="margin: 10px;"></div><div class="harga-barang">$' + response[i].fields.price + '</div><div style="margin: 20px;"></div><hr><div style="margin: 18px;"></div><div class="item-status">' + response[i].fields.status + '</div></div></div></a>';
                        //trHTML += '<a href="{% url \'description:desc\' id=' + response[i].pk + ' title=' + response[i].fields.title + ' %}">' + '<div class="card-barang"><img src="/media/' + response[i].fields.image + '" width="230" height="180" class="gambar"/><h2><a href="{% url \'description:desc\' id=' + response[i].pk + ' title=' + response[i].fields.title + ' %}">' + response[i].fields.title + '</a></h2><h3>$' + response[i].fields.price + '</h3><div class="item-status">' + response[i].fields.status + '</div></div></a>';
                    }

                    trHTML += '</div>';

                    $('#result').append(trHTML);
                }
            }
        });
    });
});