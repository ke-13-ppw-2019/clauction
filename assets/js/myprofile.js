$(document).ready(() => {
    $('#history').on({
        click: function(){
        var getUrl = 'https://clauction.herokuapp.com/getDataAll'

        console.log("clicked");
        $.ajax({
                method: 'GET',
                url: getUrl,
                success: function(response) {
                    $('#result').empty();
                    if (response.length == 0) {
                        var trHTML = '<div style="margin: 30px;">';
                        trHTML += '<div style="text-align: center;"><h3>No items found!</h3></div></div>';
                        $('#result').append(trHTML);
                    } else {

                        var trHTML = '<div class="flex-wrap">';
                        for (let i = 0; i < response.length; i++) {
                            trHTML += '<a href="/' + response[i].pk + '/' + response[i].fields.title + '">' + '<div class="card-barang"><img src="/media/' + response[i].fields.image + '" width="250" height="180" class="gambar" style="margin-top: 0; border-radius: 20px 20px 0px 0px;"/><div class="info-barang"><h2><a href="/' + response[i].pk + '/' + response[i].fields.title + '"><b>' + response[i].fields.title + '</b></a></h2><h3>$' + response[i].fields.price + '</h3><div class="item-status">' + response[i].fields.status + '</div></div></div></a>';
                            
                        }

                        trHTML += '</div>';

                        $('#result').append(trHTML);
                    }
                   
                }
            });
        }
    });
});