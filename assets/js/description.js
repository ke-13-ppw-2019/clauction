function displayForm(id) {
    document.getElementById("ansform " + id).style.display = "flex";
    document.getElementById("replytxt " + id).innerHTML = "";
}

$('#placebid').on({
    click: function(){
        $('#overlay_bidform').css("display", "flex");
    },
}); 

$('#bid_form').on({
    mouseleave: function(){
        $('#overlay_bidform').css("display", "none");
        $('#thanks_content').css("display", "none");
    },
}); 

$(document).on('submit', '#bid_form', function() {
    $.ajax({
        method: 'POST',
        url : 'http://clauction.herokuapp.com/bid' ,
        data: {
            item_id : $('input[name=item_id]').val(), 
            price : $('#id_price').val(), 
            csrftoken: $('input[name=csrfmiddlewaretoken]').val(),
        },
        success: function(response) {
            $('#thanks_content').css("display", "flex");
            $('#overlay_bidform').css("display", "flex");
        },
    });
}); 