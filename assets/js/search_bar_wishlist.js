$(document).ready(() => {
    $('#search-bar').on("keyup", function () {
        console.log('pressed')
        var query = $('#search-bar').val();
        var getUrl = '';

        if (query.length == 0) {
            getUrl = 'http://clauction.herokuapp.com/getDataAllWishlist/'
        } else {
            getUrl = 'http://clauction.herokuapp.com/getDataWishlist/?q=' + query
        }
        console.log('query = ' + query + ', panjang = ' + query.length);
        console.log('url = ' + getUrl);
        $.ajax({
            method: 'GET',
            url: getUrl,
            success: function (response) {
                $('#result').empty();

                if (response.length == 0) {
                    var trHTML = '<div style="margin: 30px;">';
                    trHTML += '<div id="nothing" style="text-align: center;"><h3>No items found!</h3></div></div>';
                    $('#result').append(trHTML);
                } else {
                    var trHTML = '<div class="container">';
                    for (let i = 0; i < response.length; i++) {
                        trHTML += 
                        '<table><tr><td><img src="/media/' + response[i].fields.item.image + '" width="120" height="150" class="gambar" style="margin-top: 0; margin-right: 100; border-radius: 10px 10px 10px 10px;" /></td><td> <h2><a href="/' + response[i].pk + '/' + response[i].fields.item_name + '">' + '<b>'+ response[i].fields.item_name + '</b></a>';
                    }
                    trHTML += '</div>';

                    $('#result').append(trHTML);
                }
            }
        });
    });
});