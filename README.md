# Clauction

[![pipeline status](https://gitlab.com/ke-13-ppw-2019/clauction/badges/master/pipeline.svg)](https://gitlab.com/ke-13-ppw-2019/clauction/commits/master)

[![coverage report](https://gitlab.com/ke-13-ppw-2019/clauction/badges/master/coverage.svg)](https://gitlab.com/ke-13-ppw-2019/clauction/commits/master)

A Django based web application to fulfill PPW assignment.

# Authors
[Glenda Emanuella Sutanto](https://gitlab.com/glendaesutanto)

[Wulan Mantiri](https://gitlab.com/wulanmantiri_)

[Vikih Fitrianih](https://gitlab.com/VikihFitrianih)

[Amalia Hanisafitri](https://gitlab.com/amaliahanisa)

# About Clauction
Clauction is an online platform to buy and sell properties which follows auction procedures. Clauction guarantees accessibility, credibility, and comfortability for any transaction between buyers and sellers.

# Features
Clauction offers 5 features:

1. Home (by Glenda E Sutanto)

    Home is first page of Clauction that displays many product options with their image, title, and price. Sort Feature is available for buyers to narrow and specify their needs based on pricing, time limit, and alphabet order. Users also can search for their wanted items and write their reviews.

2. Description (by Wulan Mantiri)

    Clicking one of the product options in Home will direct to Description Feature, which contains details on the auction and payment along with item description and other additional information. Question and Answer (QnA) Feature is offered for transparency between buyers and sellers.

3. Profile (by Vikih Fitrianih)

    Profile page provides general biodata of the user. It also displays the history where the user can revisit their previous selling products. 

4. Wishlist (by Amalia Hanisafitri)

    Wishlist page is the page that contains a list where user has tagged items to be added to the wishlist from the Description page.

5. Selling Form

    The product options are obtained from the data filled by users as sellers in Selling Form. Data such as user's bank account and name are  mandatory to transfer the money obtained after the auction finishes. As for item information, users are required to provide the title, end date, starting price, description, and image.


# Heroku Link
[clauction.herokuapp.com](http://clauction.herokuapp.com/)