from django.test import TestCase, Client
from .models import Item
from django.contrib.auth.models import User
from django.core.files.uploadedfile import InMemoryUploadedFile
from io import BytesIO
from PIL import Image
import datetime

class SellingFormUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        User.objects.create(
            first_name = "Wulan", 
            last_name = "Mantiri",
            username = "wulanmantiri_",
            password = "qwertyuiop098",
            email = "wulan.mantiri@gmail.com",)
    
    def test_url_exists(self):
        response = self.client.get('/sell/')
        self.assertEqual(response.status_code, 302)

    def test_create_new_item_model(self):
        Item.objects.create(
            user = User.objects.get(username = "wulanmantiri_"),
            id = 1,  
            bank_name = "BCA", 
            bank_account = "12345678",
            title = "chevrolet", 
            start_date = datetime.datetime.now(), 
            end_date = "2019-12-31 17:00:00+00:00",
            starting_price = "15", 
            price="15", 
            image = "assets/ferrari.jpg", 
            description = "good", )
        self.assertEqual(len(Item.objects.all()), 1)

#     def test_save_POST_request_when_item_field_is_filled(self):
#         image = Image.new(mode='RGB', size=(200, 200))
#         img_io = BytesIO()
#         image.save(img_io, 'JPEG')
#         image.encode('utf-8')
#         # 
#         img = InMemoryUploadedFile(img_io, None, "ferrari.jpg", "image/jpeg", len(img_io.getvalue()), None)
#    #     img = open("assets/ferrari.jpg", "rb")
#         response = self.client.post('/sell/',
#             data= {'user': User.objects.get(id=1),   
#                 'bank_name': "BCA", 
#                 'bank_account': "12345678",
#                 'title': "chevrolet", 
#                 'start_date': datetime.datetime.now(), 
#                 'end_date': "2019-12-31 17:00:00+00:00",
#                 'starting_price': "15", 
#                 'price': "15", 
#                 #'image': img, 
#                 'description': "good",})
#         self.assertEqual(len(Item.objects.all()), 1)
#         self.assertEqual(response.status_code, 200)