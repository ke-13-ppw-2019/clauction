from django.apps import AppConfig


class SellingformConfig(AppConfig):
    name = 'sellingform'
