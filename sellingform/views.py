from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Item
from . import forms
from django.contrib.auth.decorators import login_required

@login_required(login_url="/login")
def sell_item(request):
    if request.method == 'POST':
        form = forms.SellerForm(request.POST, request.FILES)
        if form.is_valid():
            Item.objects.create(
                user = request.user,
                bank_name = request.POST.get('bank_name'), 
                bank_account = request.POST.get('bank_account'),
                title = request.POST.get('title'), 
                end_date = request.POST.get('end_date'),
                starting_price = request.POST.get('starting_price'), 
                price= request.POST.get('starting_price'), 
                image = request.FILES.get('image'), 
                description = request.POST.get('description'), )
            title = form.cleaned_data['title']
            end_date = form.cleaned_data['end_date']
            return render(request, 'thanks.html', {
                'title':title,'end_date':end_date,
                'navlink' : reverse('sell'),
                'sellorbuy' : 'Sell', })
    else:
        form = forms.SellerForm()
    return render(request, 'sell.html', {
        'form': form,
        'navlink' : reverse('home:home'),
        'sellorbuy' : 'Buy',})

