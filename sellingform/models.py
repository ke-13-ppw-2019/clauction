from django.db import models
from django.contrib.auth.models import User

class Item(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="items")
    bank_name = models.CharField(max_length=50)
    bank_account = models.CharField(max_length=20)

    title = models.CharField(max_length=100)
    start_date = models.DateTimeField(auto_now_add=True)
    end_date = models.DateTimeField()
    starting_price = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to="images", blank=True)
    description = models.TextField()

    price = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=15,default='Available')

    #def __str__(self):
     #   return self.userprofile.name