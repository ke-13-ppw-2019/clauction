from django import forms
from .models import Item

class DateInput(forms.DateInput):
    input_type='date'

class SellerForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ['title', 'end_date', 'starting_price', 'image', 'description', 'bank_name', 'bank_account']
        widgets = {
            'end_date': DateInput(),
            'starting_price' : forms.NumberInput(attrs={'type': 'number'}),
            'agree' : forms.CheckboxInput(attrs={'type': 'checkbox'}),
        }
        labels = {
           # 'agree' : "I have agreed to the terms and conditions.",
            'starting_price' : "Starting price"
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
