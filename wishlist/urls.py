from . import views
from django.urls import path
from .views import add_to_wishlist, show_wishlist, parseData, getData, parseDataAll, getDataAll

app_name = 'wishlist'

urlpatterns = [
    path('add_to_wishlist/<int:id>', add_to_wishlist, name="add_to_wishlist"),
    path('my_wishlist', show_wishlist , name="show_wishlist"),
    path('parseDataWishlist/<str:query>',parseData,name='parseData'),
    path('getDataWishlist/',getData,name='getData'),
    path('parseDataAllWishlist/',parseDataAll,name='parseDataAll'),
    path('getDataAllWishlist/',getDataAll,name='getDataAll'),
]