from django.test import TestCase, Client
from .models import Wishlist
from sellingform.models import Item
from django.contrib.auth.models import User
import datetime
from django.contrib import auth

class WishlistUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        User.objects.create(
            first_name = "Wulan", 
            last_name = "Mantiri",
            username = "wulanmantiri_",
            password = "qwertyuiop098",
            email = "wulan.mantiri@gmail.com",)
        Item.objects.create(
            user = User.objects.get(username = "wulanmantiri_"),
            id = 1,  
            bank_name = "BCA", 
            bank_account = "12345678",
            title = "chevrolet", 
            start_date = datetime.datetime.now(), 
            end_date = "2019-12-31 17:00:00+00:00",
            starting_price = "15", 
            price="15", 
            image = "assets/ferrari.jpg", 
            description = "good", )
        Item.objects.create(
            user = User.objects.get(username = "wulanmantiri_"),
            id = 2,
            bank_name = "bca", 
            bank_account = "12345678",
            title = "avanza", 
            start_date = datetime.datetime.now(), 
            end_date = "2018-12-31 17:00:00+00:00",
            starting_price = "25", price="25", 
            image = "assets/ferrari.jpg", 
            description = "very good", )
        self.client.force_login(User.objects.get(username="wulanmantiri_"))

    def test_url_exists(self):
        response = self.client.get('/my_wishlist')
        self.assertEqual(response.status_code, 200)

    def test_create_wishlist(self):
        wishlist = Wishlist.objects.create(
			item = Item.objects.get(id=1),
			user_profile = User.objects.get(username = "wulanmantiri_"),
		)
        self.assertEqual(len(Wishlist.objects.all()), 1)
    
    