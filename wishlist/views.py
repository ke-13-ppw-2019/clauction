from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.shortcuts import render, redirect, get_object_or_404
from sellingform.models import Item
from .models import Wishlist
from django.http import HttpResponse, JsonResponse
from django.core import serializers
import requests
import datetime

@login_required()
def add_to_wishlist(request, id):
    user_profile = request.user
    item =Item.objects.get(id=id)
    if Wishlist.objects.filter(user_profile=user_profile, item=item, item_name=item.title).exists():
        messages.warning(request, f'Item already exists')
        # return redirect(reverse('description:desc'))
        return redirect('description:desc', id, item.title)
    Wishlist.objects.get_or_create(item = item, user_profile= user_profile, item_name=item.title)
    messages.success(request, f'Item has been added to the wishlist')
    return redirect('description:desc', id, item.title)

@login_required()
def show_wishlist(request):
    user_profile = request.user
    wishlists = user_profile.wishlists.all()
    return render(request, 'wishlist.html', {'wishlists' : wishlists})

def parseData(request, query):
    queryResult = []

    for wishlist in Wishlist.objects.filter(user_profile=request.user):
        if(query.lower() in wishlist.item_name.lower()):
            queryResult.append(wishlist)

    wishlist_list = serializers.serialize('json',queryResult)
    return HttpResponse(wishlist_list, content_type="application/json")

def parseDataAll(request):
    item = Wishlist.objects.all()

    item_list = serializers.serialize('json',item)
    return HttpResponse(item_list, content_type="application/json")

def getDataAll(request):
    url = 'http://clauction.herokuapp.com/parseDataAllWishlist/'

    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)

def getData(request):
    q = request.GET['q']
    url = 'http://clauction.herokuapp.com/parseDataWishlist/' +  q
    response = requests.get(url)
    response_json = response.json()
    return JsonResponse(response_json, safe=False)