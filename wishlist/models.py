from django.db import models
from sellingform.models import Item
from django.contrib.auth.models import User


# Create your models here.
class Wishlist(models.Model):
    user_profile = models.ForeignKey(User, on_delete=models.CASCADE, related_name="wishlists")
    item = models.ForeignKey(Item, on_delete=models.CASCADE,null=True)
    item_name = models.CharField(max_length=100, default='')


    def __str__(self):
        return "{} -- {}".format(self.user_profile.username, self.item.title)