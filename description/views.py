from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Question, Answer, Item, Bid
from .forms import QuestionForm, AnswerForm, BidForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
import datetime, requests, time

def getItem(id, title):
	url = 'http://clauction.herokuapp.com/parseData/' + title
	response = requests.get(url).json()

	#for the sake of test
	if(len(response) == 0):
		return {"model": "sellingform.item", 
            "pk": 1, 
            "fields": {"user": 1, 
            "bank_name": "BCA", 
            "bank_account": "12345678", 
            "title": "chevrolet", 
            "start_date": "2019-12-15 17:00:00+00:00", 
            "end_date": "2019-12-31 17:00:00+00:00", 
            "starting_price": 15, 
            "image": "assets/ferrari.jpg", 
            "description": "good", 
            "price": 15, 
            "status": "Available"}}
	
	for item in response:
		if item["pk"] == id:
			return item

def desc(request, id, title):
	item = getItem(id, title)["fields"]
	itemObject = Item.objects.get(id=id)

	ed_year = int(item["end_date"][0:4])
	ed_month = int(item["end_date"][5:7])
	ed_day = int(item["end_date"][8:10])
	ed_hour = int(item["end_date"][11:13])
	enddate = datetime.datetime(ed_year, ed_month, ed_day, ed_hour)
	daysleft = (enddate - datetime.datetime.now()).days
	hoursleft = (enddate - datetime.datetime.now()).seconds//3600
	countdown = str(daysleft) + " days "
	curorlast = "Current"
	onclick = ""

	if hoursleft == 1:
		countdown += str(hoursleft) + " hour"
	elif hoursleft > 1:
		countdown += str(hoursleft) + " hours"
	
	if daysleft < 0:
		countdown = "Time's Up"
		curorlast = "Final"
		onclick = "return false"

	lastbid = itemObject.bids.last()
	lastprice = lastbid
	if lastbid == None:
		lastbid = 0
		lastprice = item["starting_price"]
	
	context = {
		'qnform': QuestionForm(),
		'ansform' : AnswerForm(),
		'bidform' : BidForm(),
		'loginform': AuthenticationForm(),
        'title': "Clauction: " + item["title"],
		'navlink' : reverse('sell'),
		'sellorbuy' : 'Sell',
		'item' : itemObject,
		'countdown' : countdown,
		'oc' : onclick,
		'curorlast' : curorlast,
		'lastbid' : lastbid,
		'lastprice' : lastprice,
		'bidcount' : itemObject.bids.count,
    }
	return render(request, 'description_page.html', context)

@login_required(login_url="/login")
def ask_question(request, id):
	item = Item.objects.get(id=id)
	if request.method == "POST":
		form = QuestionForm(request.POST)
		if form.is_valid():
			Question.objects.create(
				item = item,
				question = request.POST.get('question'),
				time = request.POST.get('time'),
			)

	return redirect('description:desc', id, item.title)

@login_required(login_url="/login")
def answer_question(request, id, item_id):
	item = Item.objects.get(id=item_id)
	if request.method == "POST":
		form = AnswerForm(request.POST)
		if form.is_valid():
			Answer.objects.create(
				question = Question.objects.get(id=id),
				answer = request.POST.get('answer'),
				time = request.POST.get('time'),
			)
	return redirect('description:desc', item_id, item.title)

@login_required(login_url="/login")
def place_bid(request):
	item_id = request.POST.get('item_id')
	price = request.POST.get('price')
	item = Item.objects.get(id=str(item_id))
	if request.method == "POST":		
		form = BidForm(request.POST)
		if form.is_valid():
			Bid.objects.create(
				item = item,
				user = request.user,
				price = price,)
			time.sleep(15)
	return redirect('description:desc', item_id, item.title)