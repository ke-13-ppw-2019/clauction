from django import forms
from .models import Bid
from django.forms import ModelForm

qn_attrs = {
        'class': 'form-control',
        'placeholder': 'message/question',
        }

ans_attrs = {
        'class': 'form-control',
        'placeholder': 'Write your reply',
        }

class QuestionForm(forms.Form):
    question = forms.CharField(label="", required=True, max_length=400, widget=forms.TextInput(attrs=qn_attrs))

class AnswerForm(forms.Form):
    answer = forms.CharField(label="", required=True, max_length=400, widget=forms.TextInput(attrs=ans_attrs))

class BidForm(ModelForm):
    class Meta:
        model = Bid
        fields = ['price']
        labels = {'price':'Price'}
        widgets = {
            'price':forms.NumberInput(attrs={'class':'form-control','type':'number'}),
            }