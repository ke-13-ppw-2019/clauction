from django.db import models
from sellingform.models import Item
from django.contrib.auth.models import User

class Question(models.Model):
	item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="questions")
	question = models.CharField(max_length=400, blank=False)
	time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.question

class Answer(models.Model):
	question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="answers")
	answer = models.CharField(max_length=400, blank=False)
	time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.answer

class Bid(models.Model):
	item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="bids")
	user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="mybids")
	price = models.PositiveIntegerField(default=0)
	
	def __str__(self):
		return str(self.price)